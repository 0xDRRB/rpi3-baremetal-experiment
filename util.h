#pragma once

extern int get_el(void);
extern uint64_t get_sctlr_el1(void);
extern uint64_t get_sctlr_el2(void);
extern uint64_t get_sctlr_el3(void);

void printhex(unsigned int d);
void printhex64(uint64_t d);
int strlen(char s[]);
void reverse(char s[]);
void itoa(unsigned int n, char s[]);
void printdec(unsigned int d);
void puts(const char *s);

